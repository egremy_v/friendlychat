import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-messaging',
  templateUrl: './messaging.component.html',
  styleUrls: ['./messaging.component.css']
})

export class MessagingComponent implements OnInit {
  itemsRef: AngularFireList<any>;
  items: Observable<any[]>;

  constructor(afDatabase: AngularFireDatabase) {
    this.itemsRef = afDatabase.list('messages');
    this.items = this.itemsRef.snapshotChanges().pipe(
      map(changes =>
        changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
      )
    );
  }

  sendMessage(newMessage: string) {
    this.itemsRef.push({ text: newMessage });
  }

  ngOnInit() {
  }

}

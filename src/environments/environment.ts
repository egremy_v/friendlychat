// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBB1QwEdAJBJVuePYCk19CWM5prEySDrPs',
    authDomain: 'friendlychat-43f52.firebaseapp.com',
    databaseURL: 'https://friendlychat-43f52.firebaseio.com',
    projectId: 'friendlychat-43f52',
    storageBucket: 'friendlychat-43f52.appspot.com',
    messagingSenderId: '497553428480'
  }
};

/*
 * In development mode, for easier debugging, you can ignore zone related error
 * stack frames such as `zone.run`/`zoneDelegate.invokeTask` by importing the
 * below file. Don't forget to comment it out in production mode
 * because it will have a performance impact when errors are thrown
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
